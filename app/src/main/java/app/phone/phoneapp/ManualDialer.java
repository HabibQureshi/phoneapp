package app.phone.phoneapp;

import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class ManualDialer extends AppCompatActivity implements View.OnClickListener {

    private TextView contactNumber;
    private ImageView call,back;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manual_dialer);
        contactNumber = findViewById(R.id.contactNumber);
        call = findViewById(R.id.call);
        back = findViewById(R.id.back);
        call.setOnClickListener(this);
        back.setOnClickListener(this);

    }



    public void back(View view) {
        String dialerText = contactNumber.getText().toString();
        if(dialerText.length()>0){
            contactNumber.setText(dialerText.substring(0,dialerText.length()-1));
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.call:
                if(contactNumber.getText().toString().length() == 11){
                    Intent i = new Intent(this,Calling.class);
                    i.putExtra("num",contactNumber.getText().toString());
                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(i);
                }
                else Toast.makeText(this,"Contact length must be 11",Toast.LENGTH_LONG).show();
                break;
            case R.id.back:
                back(view);
                break;

        }
    }

    public void numberClick(View v){
        String contactNum =((TextView)v).getText().toString();
        String dialerText = contactNumber.getText().toString();
        if(dialerText.length() == 0 && Integer.parseInt(contactNum) != 0) {
            Toast.makeText(this, "Number can only start with '0'", Toast.LENGTH_LONG).show();
        }
        else contactNumber.setText(contactNumber.getText().toString()+contactNum);

    }
}
