package app.phone.phoneapp;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.util.List;


public class Adapter extends RecyclerView.Adapter<Adapter.ViewHolder> {

    private View layout;
    private LayoutInflater inflater;
    private List<Contact> contacts;
    private MainActivity activity;
    private PhoneData data;

    public Adapter(Context context) {
        if(context instanceof MainActivity) {
            this.activity = (MainActivity) context;
            data = new PhoneData(activity);     // phone contacts initlization
            this.setContacts(data.getAll());
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        this.inflater = (LayoutInflater) parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        layout = inflater.inflate(R.layout.layout, parent, false);
        ViewHolder holder = new ViewHolder(layout);

        return holder;
    }




    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {  // showing phone data
        holder.name.setText(contacts.get(position).getName());
        holder.address.setText(contacts.get(position).getAddress());
        holder.phone.setText(contacts.get(position).getPhoneNumber());

    }

    @Override
    public int getItemCount() {
         if(contacts!=null)
            return contacts.size();
         else return 0;
    }

    public void setContacts(List<Contact> contacts) {
        this.contacts = contacts;
    }



    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        TextView name, address, phone;
        Button call,edit,delete;
        public ViewHolder(View itemView) {
            super(itemView);  // binding data
            name =  itemView.findViewById(R.id.name);
            address = itemView.findViewById(R.id.address);
            phone = itemView.findViewById(R.id.phone);
            call =  itemView.findViewById(R.id.call);
            edit = itemView.findViewById(R.id.edit);
            delete = itemView.findViewById(R.id.delete);
            call.setOnClickListener(this);
            edit.setOnClickListener(this);
            delete.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            switch (view.getId()){
                case R.id.delete:   // delete phone and show updated data on screen
                    data.delete(contacts.get(getAdapterPosition()));
                    showLatestData();
                    break;
                case R.id.edit: // edit phone
                    PopUp popUp = new PopUp(activity,contacts.get(getAdapterPosition()));
                    popUp.show();
                case R.id.call: // call phone number
                    call(getAdapterPosition());

                    break;

            }

        }
    }
    public void showLatestData(){ // show latest data

        this.setContacts(data.getAll());
        this.notifyDataSetChanged();
    }
    public void call(int i){ // show calling screen
        activity.call(contacts.get(i).getPhoneNumber());
    }
}
