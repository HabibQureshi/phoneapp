package app.phone.phoneapp;


public class Constant {  // database configs
    public static final String DB_NAME = "phone_contact";
    public static final int DB_VERSION = 1;
    public static final String TABLE_NAME = "contacts";
    public static final String COLUMN_ID ="id";
    public static final String COLUMN_NAME ="name";
    public static final String COLUMN_ADDRESS = "address";
    public static final String COLUMN_NUMBER ="number";
    public static final String selectQuery =
            "SELECT  * FROM " + TABLE_NAME +
                    " ORDER BY " +
                    COLUMN_NAME;
    public static final String CREATE_TABLE =
            "CREATE TABLE "+TABLE_NAME+" ("
                    + COLUMN_ID +" INTEGER PRIMARY KEY AUTOINCREMENT,"
                    + COLUMN_NAME +" VARCHAR(30),"
                    + COLUMN_ADDRESS +" VARCHAR(100),"
                    + COLUMN_NUMBER +" VARCHAR(30)"
                    + ")";
}
