package app.phone.phoneapp;

import android.Manifest;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private RecyclerView recyclerView;
    private Adapter adapter;
    private Button addNewContact,dialer;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);   // binding views
        setContentView(R.layout.activity_main);
        recyclerView = findViewById(R.id.recycleView);
        adapter = new Adapter(this);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        addNewContact = findViewById(R.id.addNewContact);
        addNewContact.setOnClickListener(this);
        dialer = findViewById(R.id.dailer);
        dialer.setOnClickListener(this);
    }
    public void showLatestData(){
        adapter.showLatestData();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.addNewContact:  // add new contact
                PopUp popUp = new PopUp(this,null);
                popUp.show();
                break;
            case R.id.dailer: // dial contact
                Intent i = new Intent(this, ManualDialer.class);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(i);
                break;
        }

    }


    public void call(String phoneNumber) { // show fake calling screen
        Intent i = new Intent(this,Calling.class);
        i.putExtra("num",phoneNumber);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(i);
    }
}
