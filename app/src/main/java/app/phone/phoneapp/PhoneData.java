package app.phone.phoneapp;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;


public class PhoneData {
    private Context context;
    private PhoneDataBase phoneDataBase;

    public PhoneData(Context context) {
        this.context = context;
        this.phoneDataBase = new PhoneDataBase(context);
    }

    public long add(Contact contact){ // add new contact

        SQLiteDatabase db  = phoneDataBase.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(Constant.COLUMN_NAME,contact.getName());
        contentValues.put(Constant.COLUMN_ADDRESS,contact.getAddress());
        contentValues.put(Constant.COLUMN_NUMBER,contact.getPhoneNumber());
        long id = db.insert(Constant.TABLE_NAME,null,contentValues);
        db.close();
        return id;
    }
    public long update(Contact contact){ // update contact

        SQLiteDatabase db  = phoneDataBase.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(Constant.COLUMN_NAME,contact.getName());
        contentValues.put(Constant.COLUMN_ADDRESS,contact.getAddress());
        contentValues.put(Constant.COLUMN_NUMBER,contact.getPhoneNumber());
        long id = db.update(Constant.TABLE_NAME,contentValues, Constant.COLUMN_ID + " = ? ",new String[]{String.valueOf(contact.getId())});
        db.close();
        Toast.makeText(context,"Contact Updated",Toast.LENGTH_LONG).show();
        return id;
    }
    public void delete(Contact contact){ //delete contact

        SQLiteDatabase db = phoneDataBase.getWritableDatabase();
        db.delete(Constant.TABLE_NAME, Constant.COLUMN_ID +" = ? ",new String[]{String.valueOf(contact.getId())});
        db.close();
        Toast.makeText(context,"Contact Deleted",Toast.LENGTH_LONG).show();

    }
    public List<Contact> getAll(){ // get all contact form database
        List<Contact> contactList = new ArrayList<>();
        SQLiteDatabase db = phoneDataBase.getWritableDatabase();
        Cursor cursor = db.rawQuery(Constant.selectQuery, null);
            if(cursor !=null && cursor.moveToFirst()){
                do{
                    Contact contact = new Contact();
                    contact.setId(cursor.getInt(cursor.getColumnIndex(Constant.COLUMN_ID)));
                    contact.setPhoneNumber(cursor.getString(cursor.getColumnIndex(Constant.COLUMN_NUMBER)));
                    contact.setAddress(cursor.getString(cursor.getColumnIndex(Constant.COLUMN_ADDRESS)));
                    contact.setName(cursor.getString(cursor.getColumnIndex(Constant.COLUMN_NAME)));
                    contactList.add(contact);

                }while (cursor.moveToNext());
            }
        db.close();
        return contactList;
    }
}
