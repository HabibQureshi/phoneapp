package app.phone.phoneapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class Calling extends AppCompatActivity implements View.OnClickListener{

    private TextView num;
    private ImageView endCall;
    private String calling="Calling";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calling);
        num = findViewById(R.id.calling);   // binding views
        endCall = findViewById(R.id.endCall);
        endCall.setOnClickListener(this);
        Intent i = getIntent();
        if(i != null){
           Bundle bundle = i.getExtras();
           if(bundle!=null){   // show number on calling screen
              String s = bundle.getString("num");
              if(s!=null && s.length()>0)
                  calling = "Calling "+s;
           }
        }
        num.setText(calling);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.endCall:  // end call and go back to main screen
                Intent i = new Intent(this,MainActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(i);
                break;
        }
    }

    @Override
    public void onBackPressed() {

    }
}
