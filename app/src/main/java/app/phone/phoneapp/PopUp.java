package app.phone.phoneapp;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;



public class PopUp extends Dialog implements View.OnClickListener {
    private EditText name,contact,address;
    private Button cancel,add;
    private MainActivity activity;
    private Contact editContact;
    private PhoneData phoneData;

    public PopUp(@NonNull Context context, Contact contact) {
        super(context);
        if(context instanceof MainActivity) {
               activity = (MainActivity) context;
               phoneData = new PhoneData(activity);

        }
        this.editContact = contact;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pop_up); // binding vires
        name = findViewById(R.id.name);
        contact = findViewById(R.id.phone);
        address = findViewById(R.id.address);
        cancel = findViewById(R.id.cancel);
        add = findViewById(R.id.add);
        cancel.setOnClickListener(this);
        add.setOnClickListener(this);
        if(this.editContact != null){ // edit contact
            this.name.setText(editContact.getName());
            this.contact.setText(editContact.getPhoneNumber());
            this.address.setText(editContact.getAddress());
            add.setText("Update");
        }
        else add.setText("Add"); // new contact
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.add:
                Log.e("contact",contact.getText().toString());
                if(contact.getText().toString().length() == 11){
                    if((contact.getText().toString().charAt(0))=='0') { // check if contact start with 0
                        if (editContact == null) {
                            Contact newContact = new Contact();
                            newContact.setPhoneNumber(contact.getText().toString());
                            newContact.setAddress(address.getText().toString());
                            newContact.setName(name.getText().toString());
                            phoneData.add(newContact);  // add new contact

                        } else {
                            editContact.setPhoneNumber(contact.getText().toString());
                            editContact.setAddress(address.getText().toString());
                            editContact.setName(name.getText().toString());
                            phoneData.update(editContact); // update existing contact
                        }
                        activity.showLatestData();
                        reset();
                        dismiss();
                    }else  Toast.makeText(activity,"start with 0",Toast.LENGTH_LONG).show();
                }
                else Toast.makeText(activity,"length must be 11 ",Toast.LENGTH_LONG).show();
            break;
            case R.id.cancel:
                dismiss();
                break;
        }

    }

    private void reset() {
        this.name.setText("");
        this.contact.setText("");
        this.address.setText("");
    }



}
