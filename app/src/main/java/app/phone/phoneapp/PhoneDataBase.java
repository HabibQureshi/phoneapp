package app.phone.phoneapp;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;


public class PhoneDataBase extends SQLiteOpenHelper {  // database


    public PhoneDataBase(Context context) {
        super(context, Constant.DB_NAME, null, Constant.DB_VERSION);


    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(Constant.CREATE_TABLE);

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("drop table if  exists " +Constant.TABLE_NAME);
        onCreate(sqLiteDatabase);

    }






}

